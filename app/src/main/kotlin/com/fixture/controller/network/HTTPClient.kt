package com.fixture.controller.network

import com.fixture.infraestructure.repository.thesportdb.TheSportDBApi
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class HTTPClient {

    companion object {

        private const val BASE_URL = "https://www.thesportsdb.com/"
        lateinit var INSTANCE: TheSportDBApi

        fun getHTTPClient(): TheSportDBApi {
            if (!this::INSTANCE.isInitialized) {
                synchronized(TheSportDBApi::class) {
                    INSTANCE = connect()
                }
            }
            return INSTANCE
        }

        private fun connect(): TheSportDBApi {
            val okHttpClient = OkHttpClient.Builder()
                .build()

            val gson = GsonBuilder()
                .create()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(TheSportDBApi::class.java)
        }
    }
}

