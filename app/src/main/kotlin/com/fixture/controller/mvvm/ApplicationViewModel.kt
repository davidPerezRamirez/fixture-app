package com.fixture.controller.mvvm

import androidx.lifecycle.ViewModel
import com.fixture.domain.contracts.FixtureApi
import com.fixture.domain.model.Constant
import com.fixture.domain.model.Event
import com.fixture.domain.model.TournamentStage
import kotlinx.coroutines.runBlocking

class ApplicationViewModel(
    private val apiRest: FixtureApi
): ViewModel() {

    fun getEventsFromStages(stages: List<TournamentStage>): List<Event> {
        return runBlocking {
            apiRest.getEventsDetailsByTournamentStage(
                idLeague = Constant.FIFA_WORLD_CUP_ID,
                stages = stages,
                season = "2022"
            )
        }
    }
}