package com.fixture.controller.mvvm.screen

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.fixtureapp.R
import com.fixture.controller.utils.DateParser
import com.fixture.domain.model.Event
import com.fixture.domain.model.TeamResult
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.delay
import java.text.SimpleDateFormat


class StageEventsScreen(
    private val getEvents: () -> List<Event>
) {

    private var events: List<Event> = getEvents()

    fun hasEventsToDraw(): Boolean {
        return events.isNotEmpty()
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun Draw() {
        var isRefreshing by remember { mutableStateOf(false) }
        
        SwipeRefresh(
            state = rememberSwipeRefreshState(isRefreshing),
            onRefresh = {
                isRefreshing = true
                events = getEvents()
            }
        ) {
            LazyVerticalGrid(
                modifier = Modifier
                    .padding(5.dp),
                cells = GridCells.Fixed(2)
            ) {

                orderEventByWeek(events).forEach { (numberWeek, listEvent) ->
                    item(
                        span = { GridItemSpan(3) }
                    ) {
                        Card(
                            shape = RoundedCornerShape(8.dp),
                            backgroundColor = colorResource(id = getDateBackgroundColor(numberWeek)),
                            elevation = 10.dp,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(5.dp)
                        ) {
                            Text(
                                text = DateParser.starDateEndDateOfWeek(numberWeek),
                                textAlign = TextAlign.Center,
                                fontSize = 20.sp,
                                color = colorResource(id = R.color.white),
                                modifier = Modifier
                                    .padding(5.dp)
                            )
                        }
                    }

                    items(listEvent) {
                        createCardEvent(it, getEventBackgroundColor(numberWeek))
                    }

                    if (listEvent.size % 2 != 0) {
                        item {
                            Spacer(
                                modifier = Modifier
                                    .height(10.dp)
                                    .fillMaxWidth()
                            )
                        }
                    }

                }
            }

            LaunchedEffect(isRefreshing) {
                if (isRefreshing) {
                    delay(2500L)
                    isRefreshing= false
                }
            }
        }
    }

    private fun orderEventByWeek(events: List<Event>): Map<Int, List<Event>> {
        val eventsByWeek = mutableMapOf<Int, MutableList<Event>>()

        events.forEach {
            val weekOfYear = DateParser.toNumberWeek(it.dateEvent.date)

            if(eventsByWeek.containsKey(weekOfYear)) {
                eventsByWeek[weekOfYear]!!.add(it)
            } else {
                eventsByWeek[weekOfYear] = mutableListOf(it)
            }
        }

        return eventsByWeek.toSortedMap()
    }

    @SuppressLint("SimpleDateFormat")
    @Composable
    private fun createCardEvent(event: Event, color: Int) {
        Card(
            shape = RoundedCornerShape(8.dp),
            backgroundColor = colorResource(id = color),
            elevation = 10.dp,
            modifier = Modifier
                .wrapContentWidth()
                .padding(5.dp)
        ) {

            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxWidth()
            ) {

                CreateVisitorTeam(team = event.localTeam)
                CreateMathResult(localTeamScore = event.localTeam.score, visitorTeamScore = event.visitorTeam.score)
                CreateVisitorTeam(team = event.visitorTeam)
                Spacer(
                    modifier = Modifier
                        .height(10.dp)
                        .fillMaxWidth()
                )
                val formatter = SimpleDateFormat("dd/MM/yyyy")
                Text(
                    text = "${formatter.format(DateParser.toDate(event.dateEvent.date))} ${event.dateEvent.time}",
                    fontSize = 15.sp,
                    color = colorResource(id = R.color.white),
                    modifier = Modifier.padding(5.dp)
                )
            }
        }
    }

    @Composable
    private fun CreateVisitorTeam(team: TeamResult) {
        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 15.dp)
        ) {
            Text(
                team.nameTeam,
                color = colorResource(id = R.color.white),
                fontSize = 18.sp,
                modifier = Modifier
                    .padding(5.dp)
            )
            Image(
                //TODO traer escudo de equipo desde teamResult
                painter = rememberAsyncImagePainter("https://www.thesportsdb.com/images/media/team/badge/rrxutp1455461072.png"),
                contentDescription = null,
                alignment = Alignment.Center,
                modifier = Modifier
                    .size(30.dp)
                    .clip(CircleShape)
                    .border(2.dp, Color.White, CircleShape)
                    .background(colorResource(id = R.color.white))
            )
        }
    }

    @Composable
    private fun CreateMathResult(localTeamScore: Int?, visitorTeamScore: Int?) {
        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 15.dp)
        ) {
            Text(
                text = localTeamScore?.toString() ?: "0",
                fontSize = 20.sp,
                color = colorResource(id = R.color.blue_grey_500),
                modifier = Modifier
                    .background(colorResource(id = R.color.white))
                    .padding(5.dp)
                    .border(
                        border = BorderStroke(1.dp, Color.White),
                        shape = RoundedCornerShape(3.dp),
                    )
            )
            Text(
                text = "vs",
                fontSize = 18.sp,
                color = colorResource(id = R.color.white),
                modifier = Modifier.padding(5.dp)
            )
            Text(
                text = visitorTeamScore?.toString() ?: "0",
                fontSize = 20.sp,
                color = colorResource(id = R.color.blue_grey_500),
                modifier = Modifier
                    .background(colorResource(id = R.color.white))
                    .padding(5.dp)
                    .border(
                        border = BorderStroke(1.dp, Color.White),
                        shape = RoundedCornerShape(3.dp),
                    )
            )

        }
    }
    
    private fun getDateBackgroundColor(numberWeek: Int): Int {
        return if (DateParser.isCurrentWeek(numberWeek)) {
            R.color.pink_800
        } else {
            R.color.blue_grey_800
        }
    }

    private fun getEventBackgroundColor(numberWeek: Int): Int {
        return if (DateParser.isCurrentWeek(numberWeek)) {
            R.color.pink_500
        } else {
            R.color.blue_grey_500
        }
    }
}