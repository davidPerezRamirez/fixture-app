package com.fixture.controller.mvvm

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import com.fixture.controller.mvvm.screen.StageEventsScreen
import com.fixture.controller.mvvm.tabs.TabItem
import com.fixture.controller.mvvm.tabs.Tabs
import com.fixture.controller.mvvm.tabs.TabsContent
import com.fixture.domain.model.TournamentStage
import com.fixture.infraestructure.repository.thesportdb.FixtureApiRest
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState

class MainActivity : ComponentActivity() {

    private val viewModel by lazy {
        ApplicationViewModel(
            apiRest = FixtureApiRest()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ShowTabs()
        }
    }

    @OptIn(ExperimentalMaterialApi::class, ExperimentalPagerApi::class)
    @Composable
    fun ShowTabs() {
        val tabs = mutableListOf<TabItem>()
        val pagerState = rememberPagerState()

        addTab(tabs, "Fase de grupos", listOf(
                TournamentStage.GROUP_STAGE_FIRST,
                TournamentStage.GROUP_STAGE_SECOND,
                TournamentStage.GROUP_STAGE_THIRD
            )
        )
        addTab(tabs, "Octavos", listOf(TournamentStage.ROUND_OF_16))
        addTab(tabs, "Cuartos", listOf(TournamentStage.QUARTER_FINAL))
        addTab(tabs, "Semifinal", listOf(TournamentStage.SEMIFINAL))
        addTab(tabs, "Final", listOf(TournamentStage.FINAL))


        Scaffold {
            Column {
                Tabs(tabs = tabs, pagerState = pagerState)
                TabsContent(tabs = tabs, pagerState = pagerState)
            }
        }
    }

    private fun addTab(tabs: MutableList<TabItem>, title: String, tournamentStages: List<TournamentStage>) {
        val stageScreen = StageEventsScreen {
            viewModel.getEventsFromStages(tournamentStages)
        }
        if (stageScreen.hasEventsToDraw()) {
            tabs.add(TabItem(title) { stageScreen.Draw() })
        }
    }

}

