package com.fixture.controller.utils

import java.text.SimpleDateFormat
import java.time.LocalTime
import java.util.*

object DateParser {

    fun toDate(strDate: String): Date {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        
        return dateFormat.parse(strDate)
    }

    fun toNumberWeek(date: String): Int {
        val date  = toDate(date)
        val calendar = Calendar.getInstance()

        calendar.time = date

        return calendar.get(Calendar.WEEK_OF_YEAR)
    }

    fun starDateEndDateOfWeek(week: Int): String {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.WEEK_OF_YEAR, week)

        val firstDayOfWeek = calendar.firstDayOfWeek
        calendar.set(Calendar.DAY_OF_WEEK, firstDayOfWeek)
        val startDate = SimpleDateFormat(
            "dd/MM/yyyy",
            Locale.getDefault()
        ).format(calendar.time).toString()

        calendar.set(Calendar.DAY_OF_WEEK, firstDayOfWeek + 6)
        val endDate = SimpleDateFormat(
            "dd/MM/yyyy",
            Locale.getDefault()
        ).format(calendar.time).toString()

        return "$startDate - $endDate"
    }

    fun isCurrentWeek(numberWeek: Int): Boolean {
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault())
        val now = Date()
        val week = toNumberWeek(formatter.format(now))

        return numberWeek == week
    }
}