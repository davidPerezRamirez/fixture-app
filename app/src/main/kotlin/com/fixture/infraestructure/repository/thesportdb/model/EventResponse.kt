package com.fixture.infraestructure.repository.thesportdb.model

data class EventsResponse(
    val events: List<EventResponse>?
)

data class EventResponse(
    val idEvent: Int,
    val strEvent: String,
    val idHomeTeam: Int,
    val strHomeTeam: String,
    val intHomeScore: Int?,
    val idAwayTeam: Int,
    val strAwayTeam: String,
    val intAwayScore: Int?,
    val dateEvent: String,
    val strTime: String,
    val intRound: Int
)
