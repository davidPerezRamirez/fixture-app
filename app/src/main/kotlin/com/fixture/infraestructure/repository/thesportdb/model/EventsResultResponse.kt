package com.fixture.infraestructure.repository.thesportdb.model

data class EventsResultResponse(
    val results: List<EventResponse>
)
