package com.fixture.infraestructure.repository.thesportdb

import com.fixture.infraestructure.repository.thesportdb.model.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface TheSportDBApi {

    @GET("/api/v1/json/2/eventsseason.php")
    suspend fun searchEventsByIdLeagueAndSeason(
        @Query("id") idLeague: Int,
        @Query("s") season: String
    ): Response<EventsResponse>

    @GET("/api/v1/json/2/search_all_teams.php")
    suspend fun searchAllTeamsByTournamentName(
        @Query("l") tournamentName: String
    ): Response<TeamsResponse>

    @GET("/api/v1/json/2/searchevents.php")
    suspend fun searchEventsByNameAndSeason(
        @Query("e") nameEvent: String,
        @Query("s") season: String,
    ): Response<EventsDetailResponse>

    @GET("/api/v1/json/2/eventslast.php")
    suspend fun searchFiveLastEventByIdTeam(
        @Query("id") idTeam: Int
    ): Response<EventsResultResponse>

    @GET("/api/v1/json/2/eventsround.php")
    suspend fun searchEventsByTournamentStageAndSeason(
        @Query("id") idLeague: Int,
        @Query("r") idTournamentStage: Int,
        @Query("s") season: String
    ): Response<EventsResponse>
}