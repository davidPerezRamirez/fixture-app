package com.fixture.infraestructure.repository.thesportdb

import com.fixture.controller.network.HTTPClient
import com.fixture.domain.contracts.FixtureApi
import com.fixture.domain.model.*
import com.fixture.infraestructure.repository.thesportdb.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FixtureApiRest: FixtureApi {

    private val client = HTTPClient.getHTTPClient()

    override suspend fun getEvents(idLeague: Int, year: Int): List<Event>  {

        return withContext(Dispatchers.IO) {
            val events = client.searchEventsByIdLeagueAndSeason(idLeague, year.toString())

            if (events.isSuccessful) {
                return@withContext events.body()!!.let(::toDomainEvent)
            } else {
                throw Exception("Error get events: {code: ${events.code()}, cause: ${events.errorBody()}")
            }
        }

    }

    override suspend fun getTournamentTeams(nameLeague: String): List<Team> {
        return withContext(Dispatchers.IO) {
            val teams = client.searchAllTeamsByTournamentName(nameLeague)

            if (teams.isSuccessful) {
                return@withContext teams.body()!!.let(::toDomainTeam)
            } else {
                throw Exception("Error get team: {code: ${teams.code()}, cause: ${teams.errorBody()}")
            }
        }
    }

    override suspend fun getEventDetails(nameEvent: String, season: String): Event {

        return withContext(Dispatchers.IO) {

            val events = client.searchEventsByNameAndSeason(nameEvent, season)

            if (events.isSuccessful) {
                return@withContext events.body()!!.let(::toDomainEventDetail).first()
            } else {
                throw Exception("Error get events: {code: ${events.code()}, cause: ${events.errorBody()}")
            }
        }
    }

    override suspend fun getLastFiveEventByIdTeam(idTeam: Int): List<Event> {

        return withContext(Dispatchers.IO) {

            val events = client.searchFiveLastEventByIdTeam(idTeam)

            if (events.isSuccessful) {
                return@withContext events.body()!!.let(::toDomainEventResult)
            } else {
                throw Exception("Error get events: {code: ${events.code()}, cause: ${events.errorBody()}")
            }
        }
    }

    override suspend fun getEventsDetailsByTournamentStage(
        idLeague: Int,
        stages: List<TournamentStage>,
        season: String
    ): List<Event> {

        return withContext(Dispatchers.IO) {
            val events = mutableListOf<Event>()

            stages.forEach {
                val result = client.searchEventsByTournamentStageAndSeason(
                    idLeague,
                    getIdTournamentStage(it),
                    season
                )
                if (result.isSuccessful) {
                    events.addAll(result.body()!!.let(::toDomainEvent))
                } else {
                    throw Exception("Error get events: {code: ${result.code()}, cause: ${result.errorBody()}")
                }
            }

            return@withContext events
        }
    }

    private fun getIdTournamentStage(tournamentStage: TournamentStage): Int {
        return when(tournamentStage) {
            TournamentStage.GROUP_STAGE_FIRST -> 1
            TournamentStage.GROUP_STAGE_SECOND -> 2
            TournamentStage.GROUP_STAGE_THIRD -> 3
            TournamentStage.ROUND_OF_16 -> 4
            TournamentStage.QUARTER_FINAL -> 125
            TournamentStage.SEMIFINAL -> 150
            TournamentStage.FINAL -> 200
        }
    }

    private fun toDomainTeam(teams: TeamsResponse): List<Team> {
        return teams.teams.map {
            Team(
                idTeam = it.idTeam,
                name = it.strTeam,
                icon = it.strTeamBadge
            )
        }
    }

    private fun toDomainEvent(response: EventsResponse): List<Event> {
        return if (response.events == null) {
            listOf()
        } else {
            getEvents(response.events)
        }
    }

    private fun toDomainEventDetail(response: EventsDetailResponse): List<Event> {
        return getEvents(response.event)
    }

    private fun toDomainEventResult(response: EventsResultResponse): List<Event> {
        return getEvents(response.results)
    }

    private fun getEvents(events: List<EventResponse>): List<Event> {
        return events.map {
            Event(
                idEvent = it.idEvent,
                name = it.strEvent,
                localTeam = TeamResult(
                    nameTeam = it.strHomeTeam,
                    score = it.intHomeScore
                ),
                visitorTeam = TeamResult(
                    nameTeam = it.strAwayTeam,
                    score = it.intAwayScore
                ),
                dateEvent = DateEvent(
                    date = it.dateEvent,
                    time = it.strTime
                ),
                stage = getTournamentStage(it.intRound)
            )
        }
    }

    private fun getTournamentStage(intStage: Int): TournamentStage {
        return when (intStage) {
            1 -> TournamentStage.GROUP_STAGE_FIRST
            2 -> TournamentStage.GROUP_STAGE_SECOND
            3 -> TournamentStage.GROUP_STAGE_THIRD
            4 -> TournamentStage.ROUND_OF_16
            125 -> TournamentStage.QUARTER_FINAL
            150 -> TournamentStage.SEMIFINAL
            else -> TournamentStage.FINAL
        }
    }
}