package com.fixture.infraestructure.repository.thesportdb.model

data class TeamsResponse(
    val teams: List<TeamResponse>
)

data class TeamResponse(
    val idTeam: String,
    val strTeam: String,
    val strTeamBadge: String
)
