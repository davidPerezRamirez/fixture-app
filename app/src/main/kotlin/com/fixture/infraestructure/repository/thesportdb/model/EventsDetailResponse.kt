package com.fixture.infraestructure.repository.thesportdb.model

data class EventsDetailResponse (
    val event: List<EventResponse>
)