package com.fixture.domain.contracts

import com.fixture.domain.model.Event
import com.fixture.domain.model.Team
import com.fixture.domain.model.TournamentStage

interface FixtureApi {

    suspend fun getEvents(idLeague: Int, year: Int): List<Event>

    suspend fun getTournamentTeams(nameLeague: String): List<Team>

    suspend fun getEventDetails(nameEvent: String, season: String): Event

    suspend fun getLastFiveEventByIdTeam(idTeam: Int): List<Event>

    suspend fun getEventsDetailsByTournamentStage(idLeague: Int, stages: List<TournamentStage>, season: String): List<Event>
}