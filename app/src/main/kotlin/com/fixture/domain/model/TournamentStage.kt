package com.fixture.domain.model

enum class TournamentStage {

    GROUP_STAGE_FIRST,
    GROUP_STAGE_SECOND,
    GROUP_STAGE_THIRD,
    ROUND_OF_16,
    QUARTER_FINAL,
    SEMIFINAL,
    FINAL
}