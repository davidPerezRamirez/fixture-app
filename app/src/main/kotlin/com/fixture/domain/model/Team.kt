package com.fixture.domain.model

data class Team(
    val idTeam: String,
    val name: String,
    val icon: String
)
