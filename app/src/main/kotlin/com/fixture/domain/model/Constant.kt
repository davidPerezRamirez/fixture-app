package com.fixture.domain.model

interface Constant {

    companion object {

        const val FIFA_WORLD_CUP_ID = 4429
        const val FIFA_WORLD_CUP_NAME = "FIFA%20World%20Cup"

    }
}