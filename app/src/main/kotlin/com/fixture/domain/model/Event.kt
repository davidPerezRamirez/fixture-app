package com.fixture.domain.model

data class Event (
    val idEvent: Int,
    val name: String,
    val localTeam: TeamResult,
    val visitorTeam: TeamResult,
    val dateEvent: DateEvent,
    val stage: TournamentStage
)

data class TeamResult (
    val nameTeam: String,
    val score: Int?
)

data class DateEvent(
    val date: String,
    val time: String
)